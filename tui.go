package main

import (
	"fmt"
	"math"

	"bitbucket.org/uwaploe/navsvc/pkg/atlas"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

type rowView struct {
	name string
	tab  *tview.Table
}

func NewView(name string, elements int) *rowView {
	v := rowView{name: name}
	v.tab = tview.NewTable().SetBorders(false)
	v.tab.SetCell(0, 0,
		tview.NewTableCell(v.name).
			SetTextColor(tcell.ColorYellow).
			SetAlign(tview.AlignLeft))
	for i := 1; i <= elements; i++ {
		v.tab.SetCell(0, i,
			tview.NewTableCell("      ").
				SetTextColor(tcell.ColorWhite).
				SetAlign(tview.AlignRight))
	}

	return &v
}

func (v *rowView) updateFloat(vals []float64) {
	for i, val := range vals {
		v.tab.GetCell(0, i+1).SetText(fmt.Sprintf("%6.2f", val))
	}
}

func (v *rowView) updateString(vals []string) {
	for i, val := range vals {
		v.tab.GetCell(0, i+1).SetText(val)
	}
}

func (v *rowView) updateBits(vals []uint8) {
	for i, val := range vals {
		v.tab.GetCell(0, i+1).SetText(fmt.Sprintf(" %08b", val))
	}
}

func (v *rowView) getPrimitive() tview.Primitive {
	return v.tab
}

const TS_FORMAT = "2006-01-02T15:04:05.999Z07:00"

type navWidget struct {
	flex               *tview.Flex
	status             *rowView
	pos, vel, cog, ang *rowView
	alt, depth         *rowView
	gyro               *rowView
	accel              *rowView
	sv                 *rowView
	vessel             *rowView
	cable              *rowView
	posState           [10]int
	posCount           int64
}

func (w *navWidget) getPrimitive() tview.Primitive {
	return w.flex
}

func NewNavWidget(title string) *navWidget {
	w := navWidget{}
	w.status = NewView("Flags: ", 2)
	w.pos = NewView("GNSS: ", 2)
	w.vel = NewView("Velocity (NED m/s): ", 3)
	w.cog = NewView("Course (° True): ", 1)
	w.ang = NewView("Angles (Roll/Pitch/Hdg): ", 3)
	w.accel = NewView("Accel (m/s^2): ", 3)
	w.depth = NewView("Depth (m): ", 1)
	w.alt = NewView("Alt (m): ", 1)
	w.gyro = NewView("Gyro rates (φ/θ/ψ deg/s): ", 3)
	w.sv = NewView("Sound Speed (m/s): ", 1)
	w.vessel = NewView("Vessel GNSS: ", 2)
	w.cable = NewView("Cable out (m): ", 1)

	w.flex = tview.NewFlex()
	w.flex.SetBorder(true)
	w.flex.SetTitle(title)
	w.flex.SetDirection(tview.FlexRow).
		AddItem(w.status.getPrimitive(), 1, 1, false).
		AddItem(w.pos.getPrimitive(), 1, 1, false).
		AddItem(w.vel.getPrimitive(), 1, 1, false).
		AddItem(w.cog.getPrimitive(), 1, 1, false).
		AddItem(w.ang.getPrimitive(), 1, 1, false).
		AddItem(w.gyro.getPrimitive(), 1, 1, false).
		AddItem(w.accel.getPrimitive(), 1, 1, false).
		AddItem(w.depth.getPrimitive(), 1, 1, false).
		AddItem(w.alt.getPrimitive(), 1, 1, false).
		AddItem(w.sv.getPrimitive(), 1, 1, false).
		AddItem(w.vessel.getPrimitive(), 1, 1, false).
		AddItem(w.cable.getPrimitive(), 1, 1, false)

	return &w
}

func (w *navWidget) update(rec atlas.MustNavMessage) {
	status := make([]uint8, 2)
	status[0] = uint8(rec.Valid >> 8)
	status[1] = uint8(rec.Valid)
	w.status.updateBits(status)

	pos := make([]string, 2)
	if (rec.Valid & atlas.ValidPos) != 0 {
		pos[0] = fmt.Sprintf("%11.6f", rec.Lat*180./math.Pi)
		pos[1] = fmt.Sprintf("%11.6f", rec.Lon*180./math.Pi)
	} else {
		pos[0] = fmt.Sprintf("[red]%11.6f[-]", rec.Lat*180./math.Pi)
		pos[1] = fmt.Sprintf("[red]%11.6f[-]", rec.Lon*180./math.Pi)
	}
	w.pos.updateString(pos)

	if (rec.Valid & atlas.ValidVesselPos) != 0 {
		pos[0] = fmt.Sprintf("%11.6f", rec.VLat*180./math.Pi)
		pos[1] = fmt.Sprintf("%11.6f", rec.VLon*180./math.Pi)
		w.vessel.updateString(pos)
	}

	if (rec.Valid & atlas.ValidVeloOg) != 0 {
		w.vel.updateFloat([]float64{
			float64(rec.Vnorth),
			float64(rec.Veast),
			float64(rec.Vdown),
		})
		course := math.Atan2(float64(rec.Veast), float64(rec.Vnorth)) * 180. / math.Pi
		if course < 0 {
			course += 360.0
		}
		w.cog.updateFloat([]float64{course})
	}

	w.ang.updateFloat([]float64{
		float64(rec.Roll) * 180. / math.Pi,
		float64(rec.Pitch) * 180. / math.Pi,
		float64(rec.Heading) * 180. / math.Pi,
	})
	w.gyro.updateFloat([]float64{
		float64(rec.Wz),
		float64(rec.Wy),
		float64(rec.Wx),
	})
	w.accel.updateFloat([]float64{
		float64(rec.Ax),
		float64(rec.Ay),
		float64(rec.Az)})

	if (rec.Valid & atlas.ValidSv) != 0 {
		w.sv.updateFloat([]float64{
			float64(rec.Sv),
		})
	}

	if (rec.Valid & atlas.ValidDepth) != 0 {
		w.depth.updateFloat([]float64{
			float64(rec.Depth),
		})
	}

	if (rec.Valid & atlas.ValidAltitude) != 0 {
		w.alt.updateFloat([]float64{
			float64(rec.Altitude),
		})
	}

	if (rec.Valid & atlas.ValidCableOut) != 0 {
		w.cable.updateFloat([]float64{
			float64(rec.CableOut),
		})
	}
}
