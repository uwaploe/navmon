// Navmon monitors the output from the MUST navigation server
package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/uwaploe/navsvc/pkg/atlas"
	"bitbucket.org/uwaploe/navsvc/pkg/boss"
	"github.com/rivo/tview"
)

var usage = `Usage: navmon host:port

Display the MuST navigation data in real time.
`

func reader(conn net.Conn) <-chan atlas.MustNavMessage {
	ch := make(chan atlas.MustNavMessage, 1)
	pad := binary.Size(boss.JsfEdpPadding{})
	go func() {
		defer close(ch)
		scanner := boss.NewScanner(conn)
		for scanner.Scan() {
			data := scanner.Payload()
			rec := atlas.MustNavMessage{}
			err := binary.Read(bytes.NewReader(data[pad:]), boss.ByteOrder, &rec)
			if err != nil {
				break
			}
			select {
			case ch <- rec:
			default:
			}
		}
	}()
	return ch
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	args := flag.Args()

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	conn, err := net.Dial("tcp", args[0])
	if err != nil {
		log.Fatalf("Cannot connect to server %q: %v", args[0], err)
	}
	defer conn.Close()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			conn.Close()
		}
	}()

	ch := reader(conn)

	app := tview.NewApplication()
	ts := NewView("Time ", 1)
	w := NewNavWidget("Navigation Data")
	flex := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(ts.getPrimitive(), 1, 1, false).
		AddItem(w.getPrimitive(), 0, 1, true)

	go func() {
		s := make([]string, 1)
		for rec := range ch {
			t := rec.T.AsTime()
			s[0] = t.UTC().Format(TS_FORMAT)
			ts.updateString(s)
			w.update(rec)
			app.Draw()
		}
	}()

	app.SetRoot(flex, true).SetFocus(flex).Run()
}
