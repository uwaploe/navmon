module bitbucket.org/uwaploe/navmon

require (
	bitbucket.org/uwaploe/navsvc v1.2.1
	github.com/gdamore/tcell v1.2.0
	github.com/rivo/tview v0.0.0-20190829161255-f8bc69b90341
)

go 1.13
